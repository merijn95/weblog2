﻿using Weblog.ApplicationCore.Entities;
using Weblog.ApplicationCore.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Weblog.Infrastructure.Data
{
    public class EfCoreAsyncRepository<TEntity> : IAsyncRepository<TEntity> where TEntity : BaseEntity
    {
        private readonly PostContext _postContext;

        public EfCoreAsyncRepository(PostContext postContext)
        {
            _postContext = postContext;
        }

        public async Task<TEntity> AddAsync(TEntity entity)
        {
            _postContext.Set<TEntity>().Add(entity);

            await _postContext.SaveChangesAsync();

            return entity;
        }

        public Task<TEntity> DeleteAsync(TEntity entity)
        {
            throw new NotImplementedException();
        }

        public async Task<IReadOnlyList<TEntity>> FindAsync(Expression<Func<TEntity, bool>> predicate)
        {
            var result = await _postContext
                .Set<TEntity>()
                .Where(predicate)
                .ToListAsync();

            return result;
        }

        public async Task<IReadOnlyList<TEntity>> GetAllAsync()
        {
            return await _postContext.Set<TEntity>().ToListAsync();
        }

        public Task<TEntity> UpdateAsync(TEntity entity)
        {
            throw new NotImplementedException();
        }
    }
}
