﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Weblog.Infrastructure.Data.Migrations
{
    public partial class AnotherOne : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>("Summary", "Posts", "text", nullable: true);
            migrationBuilder.AddColumn<DateTime>("CreatedAt", "Posts", "datetime");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn("Summary", "Posts");
            migrationBuilder.DropColumn("CreatedAt", "Posts");
        }
    }
}
