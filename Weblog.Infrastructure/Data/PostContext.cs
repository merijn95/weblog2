﻿using Weblog.ApplicationCore.Entities.PostAggregate;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity;

namespace Weblog.Infrastructure.Data
{
    public class PostContext : DbContext
    {
        public DbSet<Post> Posts { get; set; }

        public DbSet<Comment> Comments { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) 
            => optionsBuilder
            .UseLazyLoadingProxies()
            .UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=Weblog-2;Trusted_Connection=True;MultipleActiveResultSets=true");

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Post>().HasKey(c => c.Id);
            modelBuilder.Entity<Post>()
                .HasMany(c => c.Comments)
                .WithOne()
                .HasForeignKey(b => b.Id);
        }
    }
}
