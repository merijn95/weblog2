﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Weblog.ApplicationCore.Exceptions
{
    public class TitleTooShortException : Exception
    {
        public TitleTooShortException(string message) : base(message) { }
    }
}
