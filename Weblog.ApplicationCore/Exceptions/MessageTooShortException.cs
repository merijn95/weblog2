﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Weblog.ApplicationCore.Exceptions
{
    public class MessageTooShortException : Exception
    {
        public MessageTooShortException(string message) : base(message) { }
    }
}
