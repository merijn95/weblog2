﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Weblog.ApplicationCore.Exceptions
{
    public class PostNotFoundException : Exception
    {
        public PostNotFoundException(string message) : base(message)
        {

        }
    }
}
