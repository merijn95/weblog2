﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Weblog.ApplicationCore.Exceptions
{
    public class SummaryTooShortException : Exception
    {
        public SummaryTooShortException(string message) : base(message) { }
    }
}
