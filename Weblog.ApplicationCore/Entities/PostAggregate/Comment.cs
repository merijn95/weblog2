﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Weblog.ApplicationCore.Entities.PostAggregate
{
    public class Comment : BaseEntity
    {
        public string Name { get; set; }

        public string Message { get; set; }

        public virtual Post Post { get; set; }
    }
}
