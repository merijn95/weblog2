﻿using Weblog.ApplicationCore.Exceptions;
using Weblog.ApplicationCore.Interfaces;
using Ardalis.GuardClauses;
using System;
using System.Collections.Generic;

namespace Weblog.ApplicationCore.Entities.PostAggregate
{
    public class Post : BaseEntity, IAggregateRoot
    {
        public string Title { get; set; }

        public string Summary { get; set; }

        public string Message { get; set; }

        public DateTime CreatedAt { get; set; }

        private readonly List<Comment> _comments = new List<Comment>();

        public virtual IReadOnlyList<Comment> Comments => _comments.AsReadOnly();

        public void AddComment(string name, string comment)
        {
            Guard.Against.NullOrEmpty(name, nameof(name));
            Guard.Against.NullOrEmpty(comment, nameof(comment));
          
            _comments.Add(new Comment
            {
                Name = name,
                Message = comment
            });
        }
    }
}
