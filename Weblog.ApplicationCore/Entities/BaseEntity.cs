﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Weblog.ApplicationCore.Entities
{
    public class BaseEntity
    {
        public int Id { get; set; }
    }
}
