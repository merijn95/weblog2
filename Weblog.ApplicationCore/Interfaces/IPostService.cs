﻿using Weblog.ApplicationCore.Entities.PostAggregate;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Weblog.ApplicationCore.Interfaces
{
    public interface IPostService
    {
        Task<Post> GetPostByIdAsync(int id);

        Task<Post> AddAsync(string title, string summary, string message);

        Task<IReadOnlyList<Post>> GetAllAsync();
    }
}
