﻿using Weblog.ApplicationCore.Entities.PostAggregate;
using Weblog.ApplicationCore.Exceptions;
using Weblog.ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Weblog.ApplicationCore.Services
{
    public class PostService : IPostService
    {
        private readonly IAsyncRepository<Post> _postRepository;

        public PostService(IAsyncRepository<Post> postRepository)
        {
            _postRepository = postRepository;
        }

        public async Task<Post> GetPostByIdAsync(int id)
        {
            var post = await _postRepository.FindAsync(c => c.Id == id);
            if (post.Count == 0)
            {
                throw new PostNotFoundException($"Post with id {id} not found");
            }

            return post.FirstOrDefault();
        }

        public async Task<Post> AddAsync(string title, string summary, string message)
        {
            if (title.Length < 4)
            {
                throw new TitleTooShortException("Title is too short");
            }

            if (summary.Length < 1)
            {
                throw new SummaryTooShortException("Summary is too short");
            }

            if (message.Length < 5)
            {
                throw new MessageTooShortException("Message is too short");
            }

            var entity = new Post
            {
                Title = title,
                Summary = summary,
                Message = message,
                CreatedAt = DateTime.Now
            };

            var result = await _postRepository.AddAsync(entity);

            return result;
        }


        public async Task<IReadOnlyList<Post>> GetAllAsync()
        {
            var entity = await _postRepository.GetAllAsync();

            return entity;
        }
    }
}
