﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Weblog.ApplicationCore.Entities.PostAggregate;

namespace Weblog.Web.Models
{
    public class HomeViewModel
    {
        public IReadOnlyList<Post> Posts { get; set; }
    }
}
